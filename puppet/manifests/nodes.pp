node basenode {
  include baseconfig
  # include selinux
}

node lamp inherits basenode {
  include php
  include apache
  include mysql
  include phpmyadmin
  include user
  include samba
  include git
}

