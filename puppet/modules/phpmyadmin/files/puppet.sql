-- create puppet user
CREATE USER 'puppetuser'@'localhost' IDENTIFIED BY 'puppet_user';

-- Grant permission, might change soon
GRANT ALL PRIVILEGES ON * . * TO 'puppetuser'@'localhost';

--FLUSH PRIVILEGES;
FLUSH PRIVILEGES;