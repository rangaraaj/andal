-- create database
CREATE DATABASE IF NOT EXISTS budget;

-- create user
DELETE FROM mysql.user WHERE User = 'budgetuser';
CREATE USER 'budgetuser'@'%' IDENTIFIED BY 'budgetuser';

-- grant
GRANT ALL PRIVILEGES ON *.* TO 'budgetuser'@'%' WITH GRANT OPTION;

--imp
FLUSH PRIVILEGES;