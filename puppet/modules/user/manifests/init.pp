class user {

  user::add { 'raaj':
    uid  => 2000,
    password => '$6$qgbwAAGX$G8eTT3DuwKYTankFfvb4/IFPKNYTZSiu37Yf..us9DgXwZGBIyqB9MUOuO7zhBZLqT/05yK01bEDpit7LPV7Z.'
  }

  #user::sshkey { 'raaj':
  #  key     => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDerL6dsbKzzFSfskqFQJCwP2IJ5NC/ORW3tIAGTVYbe1vWi0eeW15YgYGweVnBByxaDb9jsKOdFqvF7fimKkkZC5udRW4bkrh7hlHpqrJv6ddY7cgVinUgD+3NRhWK+R1WLsouG0bbpcLGCN1kVJ1g9mlYyAGVetwzDPuht92XJdh15zriizGurpgX7S63qKK/O1uCo3aC0cRiko/R9ZqKRiJ/WlT3Et/kwF2tI/vV6Vc0qVvGWZHworjsU2My3X2RaNoCkrBemJx/EAqoam4+QesGzKwI7OKbFPrvIRzuEf4/jedumyNXFU5LAtRokL876CWwgdP4JWaTP+pgbAmR',
  #  type    => 'ssh-rsa'
  #}

  file { "/home/raaj/.ssh/known_hosts":
    owner   => 'raaj',
    group   => 'raaj',
    mode    => 600,
    require => [ User['raaj'] ],
    source  => "puppet:///modules/user/known_hosts",
  }

  file { "/home/raaj/.ssh/id_rsa":
    owner   => 'raaj',
    group   => 'raaj',
    mode    => 600,
    require => [ User['raaj'] ],
    source  => "puppet:///modules/user/id_rsa",
  }

  file { "/home/raaj/.ssh/id_rsa.pub":
    owner   => 'raaj',
    group   => 'raaj',
    mode    => 600,
    require => [ User['raaj'] ],
    source  => "puppet:///modules/user/id_rsa.pub",
  }

  file { "/home/raaj/.ssh/authorized_keys":
    owner   => 'raaj',
    group   => 'raaj',
    mode    => 600,
    require => [ User['raaj'] ],
    source  => "puppet:///modules/user/authorized_keys",
  }
}