define user::add(
  $uid = undef,
  $password = undef,
  $groups = []
) {

  $username = $title

  group { $username:
    ensure => present,
  }

  user { $username:
    ensure    => present,
    shell     => '/bin/bash',
    uid       => $uid,
    home      => "/home/$username",
    gid       => $username,
    groups    => $groups,
    password  => $password
  }

  file {"/home/$username/":
    ensure => directory,
    owner => $username,
    group => $username,
    mode => 644,
    require => [ User[$username], Group[$username] ]
  }

  file {"/home/$username/.ssh":
    ensure => directory,
    owner => $username,
    group => $username,
    mode => 700,
    require => File["/home/$username/"],
  }

  #file { "/home/$username/.ssh/authorized_keys":
  #  ensure  => present,
  #  owner   => $username,
  #  group   => $username,
  #  mode    => 600,
  #  require => File["/home/$username/.ssh"]
  #}
}
