class mysql {
  # install apache
  package { 'mysql-server':
    ensure => present,
    require => Exec['apt-get update']
  }

  # start the service
  # service start mysql
  service { 'mysql':
    ensure  => running,
    require => Package['mysql-server'];
  }

  # set the root password
  exec { 'set-mysql-password':
    unless  => 'mysqladmin -uroot -pvagnar status',
    command => "mysqladmin -uroot password vagnar",
    path    => ['/bin', '/usr/bin'],
    require => Service['mysql'];
  }

}