class apache {
  # install apache
  package { 'apache2':
    ensure => present,
    require => Exec['apt-get update']
  }

  # start the service
  # service start apache2
  service { 'apache2':
    ensure  => running,
    require => Package['apache2'];
  }

  # apache2.conf file
  file { '/etc/apache2/apache2.conf':
    ensure  => present,
    source  => 'puppet:///modules/apache/apache2.conf',
    require => Package['apache2'],
    notify => Service['apache2']
  }

  # ports.conf file
  file { '/etc/apache2/ports.conf':
    ensure  => present,
    source  => 'puppet:///modules/apache/ports.conf',
    require => Package['apache2'],
    notify => Service['apache2']
  }
}