define git::clone ($repo, $service, $path, $username, $directory = false) {

  $source = $service ? {
    github => "git@github.com:rangaraaj/${repo}.git",
    bitbucket => "git@bitbucket.org:rangaraaj/${repo}.git",
  }

  if $directory {
    $attrs = "$source $directory"
    $creates = "$path/$directory/.git/"
  } else {
    $attrs = $source
    $creates = "$path/$repo/.git/"
  }

  exec {"git-clone-$path_$repo":
    command => "git clone $attrs",
    cwd => $path,
    path => "/usr/bin/",
    user => $username,
    group => $username,
    require => [ User[$username], Package['git'] ],
    creates => $creates,
    timeout => 0
  }
}

define git::pull($path, $username, $branch = 'master') {
  exec {"git-pull-$path":
    command => "git pull origin $branch",
    cwd => $path,
    user => $username,
    group => $username,
    require => [ User[$user] ],
    timeout => 600
  }
}
