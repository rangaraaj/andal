class samba::configure ($user, $password) {
  $shares = $samba::shares

  file { '/etc/samba/smb.conf':
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source => 'puppet:///modules/samba/smb.conf',
  }

  exec { "Setup samba user for ${user}":
    command => "/bin/echo -e \"${password}\n${password}\n\" | /usr/bin/smbpasswd -s -a ${user}",
    require => [Package['samba'], User[$user]];
  }
}
