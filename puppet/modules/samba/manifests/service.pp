class samba::service {
  $ensure = $samba::start ? {
    true => running,
    default => stopped
  }

  service { 'smbd':
    ensure => $ensure,
    enable => $samba::enable,
  }
}
