#!/bin/bash
# this script file is for creating a module for puppet

cd modules
mkdir $1
cd $1

mkdir files
mkdir manifests

cd manifests
touch init.pp